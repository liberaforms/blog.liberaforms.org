# Welcome to the blog


The main purpose of this blog is to make a post for each release, so we'll be referencing this site as a source of upgrade information.

We also expect to publish the occasional article about LiberaForms in general.

Thanks for visiting.

_Free Form software for all!_
