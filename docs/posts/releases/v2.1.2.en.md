---
date: 2022-06-11
categories:
  - Releases
authors:
  - tuttle
---

# Version 2.1.2

Critical bug fix, and others.

<!-- more -->

Fixed bugs.

* When previewing a form, multiple browser tabs could cause the form to be lost and made previously collected data nonsensical.
* User's weren't logged in after validating their email address. Fixed.

New feature.

* Optional text to display after a form is created. For example, we use this to ask for donations.

## Upgrade

```
git pull origin main
flask db upgrade
```

And restart the Flask process.
