---
date: 2022-03-30
categories:
  - Musings
authors:
  - tuttle
---

# Encryption pros and cons

Pros and cons to zero knowledge on the server

<!-- more -->

Data encryption is good, especially when dealing with other people's data.
When hosting a LiberaForms installation, all sorts of sensitive information will be stored in the database.

Currently, LiberaForms encrypts passwords, file uploads, and Fediverse credentials. These are encrytped on the server. Form answers are not encrytped.

Ideally, data submitted via the forms should not be sent to the server in-the-clear, the server should not be responsible for encrypting the data but instead, encryption/decryption should take place on the client.

Our [data-display](https://gitlab.com/liberaforms/data-display) vuejs component was, in part, developed with this (yet to come) encryption feature in mind by handling form answers on the client.

However, there are Pros and Cons to zero knowledge on the server.

**Pros**

* The person who publishes a form is the **only** person with access to the answers.
* Leaked databases or compromised servers do not reveal sensitive information.
* GDPR compliance for the system administrators is easier.

**Cons**

It becomes more (possibly much more) difficult to integrate form answers into other services, think:

* [Webhooks](https://gitlab.com/liberaforms/liberaforms/-/issues/138). Webhooks that are triggered once a form is submitted. This way an individual datum can be transferred easily and have consequences in other, possibly multiple places.
* [Programmatic retrieval](https://gitlab.com/liberaforms/liberaforms/-/issues/137) of form answers so up-to-date form answers can be reused elsewhere.
* ...

If you have any thoughts on this matter, please let us know! :)
