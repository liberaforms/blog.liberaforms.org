---
date: 2023-07-22
categories:
  - Musings
authors:
  - tuttle
---

# Form builder considerations

To build a form builder or not to build. How to best invest my time.

<!-- more -->

As the release of LiberaForms version 3 is approaching, we can start thinking about the next phases of development.

At the heart of LiberaForms is the Form builder. Since day one we have been using [formBuilder](https://formbuilder.online/). It has serverd us well, users undertand the interface and we have had no complaints about it. That's been a huge bonus!

Over the years I have added server side helper code, most notably to create unique checkbox and radio option IDs, and done some ugly hacking using it's html element classes to make it fit our needs. Meanwhile formBuilder has release new versions and some time ago I tried upgrade, but it broke LiberaForms.

So, it seemed like a good idea to develop our own form builder to write code that meets our needs and fits 'like a glove', so I wrote a formbuilder prototype in Vue.

We have been using VueJS to write our client side javascript components. Currently there are two. First the Data display component that renders form answers, and then later the GDPR wizard component (to be released with LiberaForms version 3) that helps generate Privacy policy statements.

But using frameworks has it's drawbacks. A couple of months ago when I set out to include the rendering of graphs (chartjs) into the Data display component (written in Vue2) I discovered that that meant installing legacy code. Ouch! Data display is an important part of LiberaForms and will be even more so when E2EE is implemented, so that will need to be rewritten.

Keeping that in mind, the question now is if it is wise to continue developing our VueJs form builder prototype. Building forms is a basic functionality of LiberaForms. If we use Vue, we will sooner or later need to rewrite it.

So, is it really worth the effort when the [formBuilder code](https://github.com/kevinchappell/formBuilder) works so well?

Wouldn't it be more productive if I were to learn how to contribute to the formBuilder project instead?
