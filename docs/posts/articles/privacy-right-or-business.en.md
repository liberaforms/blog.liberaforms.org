---
date: 2022-09-17
categories:
  - Articles
authors:
  - david
---

# Your privacy: right or business

Privacy is a right that affects all of us and that is systematically violated by governements and corporations. Can still “privacy first” model be a profitable business?

<!-- more -->


Privacy is a right that affects all of us and that is systematically violated, often without being much aware of it. It's easy to think of a personal data theft to access our online bank accounts or keys to access our work email, but it goes much further. On our digital day-to-day we use work tools, interact with digital leisure offers or are constantly hyperconnected by personal electronics or the ones at home. It's **in these situations that we lose control and visibility of our data being used in a way we did not know about** or that is not adjusted to the use that we were initially informed about.

![Graffiti on a wall, wandering what is privacy?](https://upload.wikimedia.org/wikipedia/commons/3/32/Privacy_2015_Graffiti.jpg)

*[KylaBorg, CC BY 2.0](https://creativecommons.org/licenses/by/2.0), via Wikimedia Commons*

Mass data capture (cookies, preferences, publications on social media...), generates an asset as a currency exchange for advertisers, for knowledge exchange between corporations, to influence the voting choice of citizens, etc. How many times have we seen a company justify the demand for personal data to improve our customer experience, but the real thing is that it applies primarily to trying to sell us more and optimize its corporate profits based on our personal data.

Often these other uses become **lucrative business, mostly legal, that takes a huge scale when falling in the hands of large corporations** such as GAFAM (Google, Apple, Facebook, Amazon, Microsoft), which are focused on their own profits. They are part of what historian Yuval Noah Harari calls “[a new kind of colonialism](https://www.eldiario.es/tecnologia/tres-consejos-harari-frenar-autoritarismo-digital_1_8083352.html)”. Harari expounded in the 2021 MWC how much of the planet is becoming a map of data colonies that transfer information to "imperial centres” in China and the United States, primarily.

So, privacy must not be taken superficially. Conscious of these pressures and frequent damages, governments try to ensure the right to privacy and, along with other entities, monitor compliance.

The right to privacy is a right already included in international consensual agreements as the United Nations Declaration of Human Rights (UDHR) of 1948, Article 12: "None shall be subject to arbitrary interference in their privacy, family, home or correspondence, or attacks on their honour and reputation. Everyone is entitled to the protection of the law against such interference or attacks”. In a clearly pre-digital era, the importance of ensuring non-interference over our privacy was already foreseen. However, technology quickly changes, new threats to the right of privacy appear in some governments, corporations, and individuals which control, intercept and collect data. Rights must evolve and adapt quickly, too.

![privacy written on a paper, about to be erased](https://upload.wikimedia.org/wikipedia/commons/a/ab/Facebook-_The_privacy_saga_continues_%284638981545%29.jpg)

*[opensource.com, CC BY 2.0](https://creativecommons.org/licenses/by/2.0), via Wikimedia Commons*


EU authorities with the [European Data Protection Regulation](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679&from=EN) (GDPR), the United Kingdom with its [Data Protection Act](https://www.legislation.gov.uk/ukpga/2018/12/contents/enacted%20) (DPA) or the state of California with its [California Consumer Privacy Act](https://www.oag.ca.gov/privacy/ccpa%20) (CCPA), among other governments, inspire new bets to modernize legislation on this right by adapting it to XXI century's background. However, governments are not perfect either, and they can regulate in an interested way by prioritizing business profits, they may arrive late or often do not have tax mechanisms to ensure compliance with specific legislation. A good example is the current status of the European GDPR, one of the last legislative contributions to ensure proper use of personal data. Despite entering into effect in May 2018, cultural change in privacy is slow, compliance is unequal and inspection is low depending on different countries, as NoYB shows in this [balance sheet of GDPR's first 4 years](https://noyb.eu/en/statement-4-years-gdpr).

<h2>When privacy is not in the centre</h2>

There are some who use privacy as a business or for their own benefit and interest. When we listen to this, we cannot help thinking about GAFAM and other digital platforms. We are all probably aware of the case of [Cambridge Analytica](https://www.theguardian.com/news/series/cambridge-analytica-files), when in 2018 it used Facebook data, without users consent, to influence elections through political announcements. One of the last movements in advertising strategy has been played by Apple. After restricting third-party advertising on their devices, [it now bets to do so itself](https://www.bloomberg.com/news/newsletters/2022-08-14/apple-aapl-set-to-expand-advertising-bringing-ads-to-maps-tv-and-books-apps-l6tdqqmg) as a source of income.

There are also governments that force legislation beyond basic rights. [The Nebraska case](https://edition.cnn.com/2022/08/10/tech/teen-charged-abortion-facebook-messages/index.html) about a mother accused of helping abort her youngest daughter due to the messages Facebook has (provided to the authorities within the framework of a federal abortion ban. Or, more recently, [about a father being falsely accused of abusing his son](https://www.nytimes.com/2022/08/21/technology/google-surveillance-toddler-photo.html) through a medically-purpose photo that was automatically synchronized to the Google cloud, resulting in authority being denounced and his account being cancelled with loss of all his personal and labour data.

We're increasingly conscientious users. According to [Entrust's privacy study](https://www.entrust.com/newsroom/press-releases/2021/data-from-entrust-reveals-contradictions-in-consumer-sentiment-toward-data-privacy-and-security), most consumers are (79%). But growing concern about data privacy does not mean that consumers are taking the necessary measures to protect their personal information. In fact, 43% said that they did not carefully review terms and conditions before downloading a new application. Talking about companies, a McAfee study points out that [only 47% of organisations know where their data is stored](https://www.mcafee.com/enterprise/en-us/assets/reports/restricted/rp-beyond-gdpr.pdf), a lot of work remains to be done.

This lack of action or control by citizens and companies facilitates, indirectly and unconsciously, this violation of the right to privacy. We agree to make use of digital platforms that do not take care of our right to privacy, while some service providers do not devote enough effort or care to guarantee this right.

Meanwhile, facing new data privacy laws, some businesses slow down their application while incorporating good privacy practices or directly searching for new ways of avoiding such pressure. Fortunately, latest verdicts in some European countries, such as [Austria against Google Analytics](https://noyb.eu/en/update-cnil-decides-eu-us-data-transfer-google-analytics-illegal), show that progress can be made in favour of privacy.

![strikers with a privacy is not a crime poster](https://upload.wikimedia.org/wikipedia/commons/e/ed/Privacy_is_not_a_crime_%284979241873%29.jpg)

*[Udo from Berlin, Deutschland, CC BY 2.0](https://creativecommons.org/licenses/by/2.0), via Wikimedia Commons*


<H2>Can “privacy first” model be a profitable business</H2>

Data protection can be a positive and distinguishing element in a business equation. At a time when the major players face in different ways rigorous and guarantor compliance within the possibilities of the right to privacy, a loophole is opening up in the market where consumers who do understand the risks seek for alternatives.

Privacy is becoming a driving factor in a client's decision to use a certain product or network, especially in sensitive services such as instant messaging, email or online forms.

Some cases that point to this trend are successful services such as: Signal encrypted messaging application, CryptPad office suite, privacy-centered ProtonMail service or Plausible web analytics.

This business focus in protecting users' privacy also offers other advantages, especially by training consumers and citizens for being responsible and active about their rights on privacy and, above all, as a political and ethical statement that technology should be in people's service.

<H2>Privacy in online forms</H2>

Today, almost all spaces and projects, regardless of whether they are collectives, associations, cooperatives or educational centers, need to collect data to carry out their activities: reservations, consultations, manifest adhesions, etc. Often, due to ignorance, privative software is used that belongs to multinational companies, which precisely base their business on the sale of our data to other companies so that they can design tailor-made advertising, specific for each of us.

Through data collection, multinational companies such as GAFAM know who we are, with whom we are, things that worry us, our joys, interests, and plans. All together, it has enabled them to be the richest companies in the world and to have unprecedented power for social manipulation. But none of the products and services of these big companies are free: we pay them with our data, which are the raw material of the dominating digital capitalism.

You should not give up the right to your own privacy just to access a digital service. That is why there are alternatives, driven by people who believe that digital technologies are an opportunity to reduce inequalities, create opportunities and guarantee this common horizon in a truly social and solidarity-based economy, far from predatory capitalisms. And that's why we create and improve common digital goods, ethical technologies, within a free culture context.

Let us build this horizon together with explicit digital ethics: let us take GAFAM out of our spaces and projects. Let us use LiberaForms to make our forms as an alternative to Google Forms.

If you want to try LiberaForms, fill in this form at [https://usem.liberaforms.org/petition](https://usem.liberaforms.org/petition)
