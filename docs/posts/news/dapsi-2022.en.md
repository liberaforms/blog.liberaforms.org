---
date: 2022-02-17
categories:
  - News
authors:
  - tuttle
---

# DAPSI grant

Data Portability and Services Incubator (DAPSI) grant

<!-- more -->

We are please to announce that we have received a grant under the European Commission’s Next Generation Internet (NGI) initiative, the Data Portability and Services Incubator (DAPSI).

During the next nine months this grant with cover:

* **Code refactoring**. After the sprint to get version 2.0 out the door, we will take some time to revise code, move things about to better organize it, rewrite some of it, and improve legibility.
* **LDAP**. Many people use LDAP to authentificate users and authorize access to resources. LDAP integration will be a great addition to LiberaForms.
* **i18n**. Internationalization of LiberaForms is going well. We will further improve it by incorporating gettext functionalities that we are not currently taking advantage of.
* **CSS**. We aim to create a unique look-and-feel for LiberaForms, and do lots of housekeeping while we are at it.
* **GDPR**. GDPR is important when publishing forms, however most people do not have the expertize to create valid GDPR texts. We intend to facilitate this process for LiberaForms' users.
* **Business strategy**. Sooner or later the grants will stop coming. We want LiberaForms to be sustainable, to at least cover maintainence and continued development costs.
* **Documentation**. We will continue to work on and improve our documentation.

[https://dapsi.ngi.eu/hall-of-fame/liberaforms/](https://dapsi.ngi.eu/hall-of-fame/liberaforms/)

Thank you NGI for this opportunity to improve LiberaForms.
